#!/bin/bash

. /etc/rc.conf
. /etc/rc.d/functions

# Set the locale
export LANG=${LOCALE:-C}
if [[ -r /etc/locale.conf ]]; then
    parse_envfile /etc/locale.conf "${localevars[@]}"
fi

declare -a pids

while read -r line
do
    pids+=("${line}")
done < <( pgrep -u pbot-ng )

case $1 in
start)
	stat_busy "Starting pbot-ng"

	# Check it's not already running.
	if ! (( ${#pids[*]} ))
	then
	    su - pbot-ng -c "cd /home/pbot-ng ; ./envbot & ./pbot-ng_fixer & ./labs_change_detector" &
	    # If it's not running then we fail.
	    if ! pgrep pbot-ng &>/dev/null
	    then
		stat_fail
		exit 1
	    fi

	    add_daemon pbot-ng
	    stat_done
	else
		stat_fail
		exit 1
	fi
	;;

stop)
	stat_busy "Stopping pbot-ng"

	if ! (( ${#pids[*]} ))
	then
	    echo "It's not running"
	    stat_fail
	    exit 1
	fi

	for (( i=0 ; i!=${#pids[*]} ; i++ ))
	do
	    kill "${pids[${i}]}" &>/dev/null ||
	    {
		stat_fail
		exit 1
	    }
	done

	unset pids

	rm_daemon pbot-ng
	stat_done

	;;

restart)
	$0 stop
	$0 start
	;;

*)
	echo "Usage: $0 {start|stop|restart}" >&2
	exit 1

esac
